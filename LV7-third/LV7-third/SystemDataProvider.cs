﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_third
{
    class SystemDataProvider : SimpleSystemDataProvider
    {
        private float previousCPULoad;
        private float previousRAMAvailable;
        public SystemDataProvider() : base()
        {
            this.previousCPULoad = this.CPULoad;
            this.previousRAMAvailable = this.AvailableRAM;
        }
        //metoda za treći
        /*public float GetCPULoad()
          {
             float currentLoad = this.CPULoad;
             if (currentLoad != this.previousCPULoad) {
             this.Notify();
             }
             this.previousCPULoad = currentLoad;
             return currentLoad;
         }*/
         //metoda za četvrti
        public float GetCPULoad()
        {
            float currentLoad = this.CPULoad;
            if (currentLoad > previousCPULoad * 1.1 || currentLoad < previousCPULoad * 0.9)
            {
                this.Notify();
            }
            this.previousCPULoad = currentLoad;
            return currentLoad;
        }
        /* metoda za treći zadatak
        public float GetAvailableRAM()
        {
            float currentLoad = this.AvailableRAM;
            if(currentLoad!=this.previousRAMAvailable)
            {
                this.Notify();
            }
            this.previousRAMAvailable = currentLoad;
            return currentLoad;
        }*/
        //metoda za četvrti zdk
        public float GetAvailableRAM()
        {
            float currentram = this.AvailableRAM;
            if (currentram > previousRAMAvailable * 1.1 || currentram < previousRAMAvailable * 0.9)
            {
                this.Notify();
            }
            this.previousRAMAvailable = currentram;
            return currentram;
        }

    }
}
