﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_third
{
    class ConsoleLogger : Logger
    {
        public void Log(SimpleSystemDataProvider provider)
        {
            Console.WriteLine(DateTime.Now + " => available RAM =" + provider.AvailableRAM + ", CPU load = " + provider.CPULoad + ".");
        }
    }
}
