﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_prvi_i_drugi
{
    class Program
    {
        static void Main(string[] args)
        {
            SortStrategy strategy = new BubbleSort();
            double[] array = { -4.7, 6.2, 7.8, -8.5, -2.987, 0.5, 5.001, 4.0987, 7.55, 13.13 };
            NumberSequence numberSequence = new NumberSequence(array);
            numberSequence.SetSortStrategy(strategy);
            Console.WriteLine(numberSequence.ToString());
            numberSequence.Sort();
            Console.WriteLine(numberSequence.ToString());

            SearchStrategy sequential = new SequentialSearch(6.2);
            numberSequence.SetSearchStrategy(sequential);
            Console.WriteLine(numberSequence.Search());
            sequential.SetElement(6.2);
            Console.WriteLine(numberSequence.Search());
        }
    }
}
