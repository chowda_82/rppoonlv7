﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_prvi_i_drugi
{
    class SequentialSearch: SearchStrategy
    {
        private double element;
        public SequentialSearch(double element)
        {
            this.element = element;
        }
        public override void SetElement(double element)
        {
            this.element = element;
        }
        public override bool Search(double[] array, int size)
        {
            int i;
            for(i=0;i<size;i++)
            {
                if(array[i]==this.element)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
