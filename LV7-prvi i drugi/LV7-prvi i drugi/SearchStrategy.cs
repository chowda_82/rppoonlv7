﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7_prvi_i_drugi
{
    abstract class SearchStrategy
    {
        public abstract bool Search(double[] array, int size);
        public abstract void SetElement(double element);
    }
}
